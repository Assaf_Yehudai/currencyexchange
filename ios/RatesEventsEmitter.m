//
//  EventsEmitter.m
//  CurrencyExchange_iOSWrapping
//
//  Created by assaf yehudai on 25/01/2021.
//  Copyright © 2021 assaf yehudai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface RCT_EXTERN_MODULE(RatesEventsEmitter, RCTEventEmitter)

RCT_EXTERN_METHOD(fetchRatesEvent)

@end
