//
//  EventsEmitter.swift
//  CurrencyExchange_iOSWrapping
//
//  Created by assaf yehudai on 25/01/2021.
//  Copyright © 2021 assaf yehudai. All rights reserved.
//

import Foundation
import React

@objc(RatesEventsEmitter)
class RatesEventsEmitter: RCTEventEmitter {
    
    public static var emitter: RCTEventEmitter!
    
    override init() {
        super.init()
        RatesEventsEmitter.emitter = self
    }
    
    override func supportedEvents() -> [String]! {
        return ["FetchRatesEventFromNative"]
    }
    
    override func startObserving() {
        print("observer set ---- 😁👍")
    }
    
    // this is required since RN 0.49+
    override static func requiresMainQueueSetup() -> Bool {
      return true
    }
}
