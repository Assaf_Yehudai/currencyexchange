import React from 'react';
import {View, SafeAreaView} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import CurrencyPicker from '../../Components/CurrencyPicker';
import RatesList from '../../Components/RatesList';
import {fetchRates} from '../../Common/Middleware/FetchingActions';

function CurrencyRates() {

    const rates = useSelector((state)=> state.rates);
    const dispatch = useDispatch();

    const onCurrencyButtonTapped = (baseCurrency) => {
        dispatch(fetchRates(baseCurrency));
    }

    return (
        <SafeAreaView style={{flex: 1, backgroundColor: '#456'}}>
            <View style={styles.container}>
                <CurrencyPicker onPress = {onCurrencyButtonTapped}/>
                <RatesList rates={rates}/>
            </View>
        </SafeAreaView>
    );
}

export default CurrencyRates;

const styles = {
    container: {
        flex: 1
    }
};
