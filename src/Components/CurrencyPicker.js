import {Button, View} from 'react-native';
import React from 'react';

const CurrencyPicker = ({onPress}) => {
    return (
        <View style={styles.pickerContainer}>
            <Button style={styles.buttonStyle} title={'USD'} onPress={() => onPress('USD')} />
            <Button style={styles.buttonStyle} title={'EUR'} onPress={() => onPress('EUR')} />
            <Button style={styles.buttonStyle} title={'ILS'} onPress={() => onPress('ILS')} />
        </View>
    );
};

export default CurrencyPicker;

const styles = {
    pickerContainer: {
        height: 100,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor:'#fff'
    },

    buttonStyle: {
        color: '#000',
    }
};
