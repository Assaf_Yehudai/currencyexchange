import {FlatList, View} from 'react-native';
import RateCell from './RateCell';
import React from 'react';

const RatesList = ({rates}) => {
    return (
        <View style={styles.list}>
            <FlatList
                data={rates}
                renderItem={({item}) => <RateCell cellItem={item} />}
                keyExtractor={(item) => item.shortName}
            />
        </View>
    );
};

export default RatesList;

const styles = {
    list: {
        flex: 1,
        backgroundColor: '#fff'
    }
};
