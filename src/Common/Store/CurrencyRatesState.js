import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';

const initialState = {
    baseCurrency: 'USD',
    rates: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_RATES': {
            const {newRates, baseCurrency} = action.payload;
            return {...state, rates: newRates, baseCurrency: baseCurrency};
        }
        default:
            return state;
    }
};

const CurrencyStore = createStore(reducer, applyMiddleware(thunk));
export default CurrencyStore;



