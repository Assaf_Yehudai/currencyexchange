import {fetchRatesBy} from '../Networking/Networking';

export const fetchRates = (baseCurrency) => async (dispatch) => {

    fetchRatesBy(baseCurrency).then (([rates, symbols]) => {
        const ratesData = mapArrays(rates.data.rates,symbols.data);
        dispatch({
            type: 'SET_RATES',
            payload: {newRates: ratesData, baseCurrency: baseCurrency}
        })
    })
}

function mapArrays(rates, symbols) {
    return Object.keys(rates).map((key) => {

        const shortName = key;
        const name = symbols[key].name;
        const country = symbols[key].countries[0] || "-";
        const symbol = symbols[key].symbol;
        const rate = rates[key];
        return { name: name, shortName: shortName, rate: rate, country: country, symbol: symbol };
    }).filter(rate => rate.country !== '-');
}
