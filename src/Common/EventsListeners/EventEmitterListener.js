import {NativeEventEmitter, NativeModules} from 'react-native';
import {useDispatch} from "react-redux";
import {fetchRatesBy} from "../Networking/Networking";

const {RatesEventEmitter} = NativeModules;
const _eventEmitter = new NativeEventEmitter(RatesEventEmitter);

_eventEmitter.addListener('FetchRatesEventFromNative',(body) => {
    const dispatch = useDispatch();
    dispatch(fetchRatesBy(body))
});

