import axios from 'axios';
import urlJoin from 'url-join';

const client = axios.create();

const baseUrls = 'https://v1.nocodeapi.com/currency_exchange_ay/cx/jkzibRxrqMvydXpP';

const endPoints = {
    rates: 'rates',
    symbols: 'symbols',
};

// MARK: -  Public Networking API
export function fetchRatesBy(baseCurrency) {
    return Promise.all([
        fetchTodaysRatesBy(baseCurrency),
        fetchSymbols()
    ]);
}

// MARK: - Private API
function fetchSymbols() {
    const url = urlJoin(baseUrls, endPoints.symbols);
    return apiCall(url, 'get');
}

export function fetchTodaysRatesBy(baseCurrency) {
    const url = urlJoin(baseUrls, endPoints.rates);

    return apiCall(url, 'get', {
        params: {
            source: baseCurrency,
        },
    });
}


//// Base Api Call
function apiCall(url, method, options) {
    const {params} = options || {};

    return client.request({
        url: url,
        method: method,
        params
    });
}
